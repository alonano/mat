import React from 'react'; 

const ProgressBar = (props) => {
    const total = !props.total ? 1 : Number(props.total);
    const greenPercentage = 100 * Number(props.green) / total; 
    const redPercentage = 100 * Number(props.red) / total; 
    let greenBar = null; 
    if (greenPercentage > 0) {
        let greenBarStyle = {
            backgroundColor: "green",
            float: "left",
            width: greenPercentage.toString() + "%",
            height: "100%"
        };
        greenBar = (<div style={greenBarStyle}>&nbsp;</div>);
    }
    let redBar = null; 
    if (redPercentage > 0) {
        let redBarStyle = {
            backgroundColor: "red",
            float: "left",
            width: redPercentage.toString() + "%",
            height: "100%"
        };
        redBar = (<div style={redBarStyle}>&nbsp;</div>);
    }
    let containerStyle = {
        width: "100%",
        height: "100%",
        position: "relative"
    };
    let centredTextStyle = {
        width: "100%",
        position: "absolute",
        fontSize: "6vh"
    };
    return (
        <div style={containerStyle}>
            <div style={centredTextStyle}>{props.text}</div>
            {greenBar} {redBar}
        </div>
    );
};

export default ProgressBar;