import React from 'react';
import ReactDOM from 'react-dom';
import ProgressBar from './ProgressBar';

describe("ProgressBar", () => {
    let div; 

    beforeEach(() => {
        div = document.createElement('div');
    });

    it("renders", () => {
        ReactDOM.render((<ProgressBar total="30" red="10" green="4" text="ABC"/>), div);
    });
});

