import React from 'react';
import soundHighScore from './Resources/high-score.mp3';
import soundHmHm from './Resources/hm-hm.mp3';
import './QuizDone.css';


const QuizDone = (props) => {
    const soundVictory = new Audio(soundHighScore);
    const soundYouCanDoBetter = new Audio(soundHmHm);
    let text; 
    let sound;
    if (props.oldHighScore < props.score) {
        sound = soundVictory;
        text = (
            <div>
                <p>High score {props.score}, BRAVO :)</p>
                <p>Old high score was {props.oldHighScore}</p>
            </div>
        )
    } else {
        sound = soundYouCanDoBetter;
        text = (
            <div>
                <p>Your score is {props.score}</p>
                <p>Old high-score {props.oldHighScore} remains.</p>
            </div>
        )
    }
    sound.play();
    return (
        <div className="quiz-done-view view">
            <div className="done-picture">PICTURE</div>
            <div className="done-text">{text}</div>
            <div className="done-buttons">
                <div 
                    className="done-button button"
                    onClick={props.backToMenu}>
                    Level selection
                </div>
                <div 
                    className="done-button button"
                    onClick={props.repeatLevel}>
                    Repeat
                </div>
            </div>
        </div>
    );
}

export default QuizDone; 