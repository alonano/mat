import React from 'react';
import ReactDOM from 'react-dom';
import QuizDone from './QuizDone';

describe("QuizDone", () => {

    xit("renders without crashing", () => {
        const div = document.createElement('div');
        ReactDOM.render((<QuizDone 
            backToMenu={() => null}
            repeatLevel={() => null}
            score="5"
            oldHighScore="6" />), div);
    });
});
