import React from 'react';
import ReactDOM from 'react-dom';
import QuizSession from './QuizSession';
import makeAddFn from '../QuizGenerator/Generators/Add';

describe("QuizSession", () => {

    it("shows the quiz session", () => {
        const div = document.createElement('div');
        const level = {
            id: "1",
            title: "10 + 10",
            description: "Adding two numbers between 0 and 10",
            timeLimit: 30,
            questionsTotal: 15,
            questionGenerator: makeAddFn(10)
        };
        ReactDOM.render((
            <QuizSession level={level} onFinish={() => 0}/> 
        ), div);
    });
});

