import React, { Component } from 'react';
import QuizView from './QuizView';
import QuizGenerator from '../QuizGenerator/QuizGenerator';
import soundCorrect from './Resources/correct.mp3';
import soundIncorrect from './Resources/incorrect.mp3';
import soundPlop from './Resources/plop.mp3';


class QuizSession extends Component {
  state = {
    generatorFn: () => QuizGenerator(this.props.level.questionGenerator),
    correctAnswers: 0,
    finalScore: 0,
    incorrectAnswers: 0,
    remainingQuestions: this.props.level.questionsTotal,
    remainingTime: this.props.level.timeLimit,
    question: QuizGenerator(this.props.level.questionGenerator),
    active: true
  }

  sounds = {
    correct: new Audio(soundCorrect),
    incorrect: new Audio(soundIncorrect),
    plop: new Audio(soundPlop)
  }

  levelCounter = setInterval(() => {
    this.setState((state, props) => {
      const remainingTime = state.remainingTime;
      if (remainingTime <= 0) {
        props.onFinish(() => state.finalScore);
        return {};   
      } else {
        return {remainingTime: remainingTime - 1};
      }
    })
  }, 1000);  

  scoreUpdateCounter = null
  
  componentWillUnmount() {
    clearInterval(this.levelCounter);
    clearInterval(this.scoreUpdateCounter);
  }

  nextQuestion = () => {
    this.setState((state, props) => {
      return {
        question: state.generatorFn()
      };
    });
  }

  finishAfterMilliseconds(milliseconds,) {
    setTimeout(() => {
      this.props.onFinish(() => this.state.finalScore);
    }, milliseconds)
  }

  levelCompleted = () => {
    this.setState({active: false});
    clearInterval(this.levelCounter);
    if (this.state.remainingTime > 0) {
      this.scoreUpdateCounter = setInterval(() => {
        this.setState((state, props) => {
          let stateChange = {};
          const newRemainingTime = state.remainingTime - 1;
          if (newRemainingTime >= 0) {
            const increasedScore = state.finalScore + 1; 
            stateChange = {remainingTime: newRemainingTime, finalScore: increasedScore};
            this.sounds.plop.play(); 
          } else {
            props.onFinish(() => state.finalScore);
          }
          return stateChange;
        });
      }, 100);
    } else {
      this.finishAfterMilliseconds(2000);
    }
  }

  noMoreQuestions = () => {
    this.setState({active: false});
    clearInterval(this.levelCounter);
    this.finishAfterMilliseconds(1000);
  }

  clickAnswerHandler = (event, index) => {
    this.setState((state, props) => {
      let stateChange = {};
      if (!state.active) {
        return stateChange;
      }
      const newRemainingQuestions = state.remainingQuestions - 1; 
      if (newRemainingQuestions < 0) {
        throw new Error("should not have negative remaining questions");
      }
      stateChange.remainingQuestions = newRemainingQuestions;
      const isCorrect = state.question.correctAnswer === index;
      if (isCorrect) {
        this.sounds.correct.play();
        const newCorrectAnswers = state.correctAnswers + 1;
        stateChange.correctAnswers = newCorrectAnswers; 
        stateChange.finalScore = newCorrectAnswers;
        if (newRemainingQuestions === 0) {
          if (state.incorrectAnswers === 0) {
            this.levelCompleted();
          } else {
            this.noMoreQuestions();
          }
        } else {
          this.nextQuestion();
        }
      } else {
        this.sounds.incorrect.play();
        const newIncorrectAnswers = this.state.incorrectAnswers + 1;
        stateChange.incorrectAnswers = newIncorrectAnswers;
        if (newRemainingQuestions === 0) {
          this.noMoreQuestions();
        } else {
          this.nextQuestion();
        }
      }
      return stateChange;
    })
  }

  render() {
    return (
      <div>
        <QuizView 
          question={this.state.question.text}
          answers={this.state.question.answers}
          click={this.clickAnswerHandler}
          totalQuestions={this.props.level.questionsTotal}
          correctAnswers={this.state.correctAnswers}
          score={this.state.finalScore}
          incorrectAnswers={this.state.incorrectAnswers}
          totalTime={this.props.level.timeLimit}
          remainingTime={this.state.remainingTime} />
      </div>
    );
  }
}

export default QuizSession;
