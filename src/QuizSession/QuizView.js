import React from 'react';
import "./QuizView.css";
import ProgressBar from '../Widgets/ProgressBar';

const QuizView = (props) => {
    return (
        <div className="quiz-view view">
            <div className="score">
                <ProgressBar 
                    total={props.totalQuestions} 
                    green={props.correctAnswers} 
                    red={props.incorrectAnswers}
                    text={props.score} />
                <ProgressBar 
                    total={props.totalTime} 
                    green={props.remainingTime}
                    text={props.remainingTime} />
            </div>
            <div className="question">
                { props.question }
            </div>
            <div className="answers">
                {props.answers.map((answer, index) => (
                    <div 
                        className="answer button"
                        key={index}
                        onClick={(event) => props.click(event, index)}>
                        {answer}
                    </div>
                ))}
            </div>
        </div>
    );    
};

export default QuizView;