import React, { Component } from 'react';
import './App.css';
import Levels from './Levels/Levels';
import LevelsMenu from './Levels/LevelsMenu';
import QuizSession from './QuizSession/QuizSession';
import QuizDone from './QuizDone/QuizDone';

class App extends Component {
  VIEW_QUIZ = 1;
  VIEW_QUIZ_DONE = 2; 
  VIEW_LEVELS_MENU = 3; 

  state = {
    view: this.VIEW_LEVELS_MENU,
    levels: Levels,
    currentLevel: null,
    highScores: {},
    score: 0,
    oldHighScore: 0
  }

  highScoreOfCurrentLevel = (state) => {
    let currentHighScore = state.highScores[state.currentLevel.id]; 
    currentHighScore = currentHighScore ? currentHighScore : 0; 
    return currentHighScore;
  }

  finishLevel = (getScore) => {
    this.setState((state, props) => {
      let stateChange = {};
      let oldHighScore = this.highScoreOfCurrentLevel(state);
      stateChange.oldHighScore = oldHighScore;
      if (getScore() > oldHighScore) {
        let updatedHighScores = {...state.highScores};
        updatedHighScores[state.currentLevel.id] = getScore(); 
        stateChange.score = getScore();
        stateChange.highScores = updatedHighScores;
      }
      stateChange.view = this.VIEW_QUIZ_DONE;
      return stateChange;
    });
  }

  chooseLevel = (levelId) => {
    this.setState((state, props) => { 
      return {
        currentLevel: state.levels.find((level) => level.id === levelId),
        view: this.VIEW_QUIZ
      };
    })
  }

  render() {
    let viewMenu = null; 
    if (this.state.view === this.VIEW_LEVELS_MENU) {
      viewMenu = (
        <LevelsMenu 
          levels={this.state.levels}
          highScores={this.state.highScores} 
          onChoose={this.chooseLevel}/>
      );
    }
    let viewQuiz = null; 
    if (this.state.view === this.VIEW_QUIZ) {
      viewQuiz = (
        <QuizSession 
          level={this.state.currentLevel} 
          onFinish={this.finishLevel} 
          key={this.state.currentLevel.id}/> 
      );
    } 
    let viewQuizDone = null; 
    if (this.state.view === this.VIEW_QUIZ_DONE) {
      const backToMenu = () => {
        this.setState({view: this.VIEW_LEVELS_MENU});
      };
      const repeatLevel = () => {
        this.setState({view: this.VIEW_QUIZ});
      }
      viewQuizDone = (
        <QuizDone 
          backToMenu={backToMenu} 
          repeatLevel={repeatLevel}
          score={this.state.score}
          oldHighScore={this.state.oldHighScore}/>
      );
    }
    return (
      <div className="App">
        {viewMenu}
        {viewQuiz}
        {viewQuizDone}
      </div>
    );
  }
}

export default App;
