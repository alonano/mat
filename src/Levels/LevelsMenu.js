import React from 'react';
import './LevelsMenu.css';

const LevelsMenuItem = (props) => {
    return (
        <div className="levels-menu-item button" 
            onClick={(event) => props.onChoose(props.id)}>
            <div className="level-menu-item-title">{props.title}</div>
            <div className="level-menu-item-high-score">{props.highScore}</div>
        </div>
    );
};

const LevelsMenu = (props) => {
    return (
        <div className="levels-menu-view view">
            <div className="levels-menu-header">Pick your level</div>
            <div className="levels-menu-level-list">
            {props.levels.map((level) => (                
                    <LevelsMenuItem 
                        title={level.title}
                        highScore={props.highScores[level.id]}
                        id={level.id}
                        key={level.id}
                        onChoose={props.onChoose} />)
            )}
            </div>
        </div>
    );
};

export default LevelsMenu; 