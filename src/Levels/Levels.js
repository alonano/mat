import makeAddFn from '../QuizGenerator/Generators/Add';
import makeSubFn from '../QuizGenerator/Generators/Sub';
import makeMultFn from '../QuizGenerator/Generators/Mult';

const Levels = [
    {
        id: "a1",
        title: "6 + 6",
        description: "Adding two numbers between 0 and 6",
        timeLimit: 30,
        questionsTotal: 20,
        questionGenerator: makeAddFn(6)
    },

    {
        id: "a3",
        title: "10 + 10",
        description: "Adding two numbers between 0 and 10",
        timeLimit: 30,
        questionsTotal: 15,
        questionGenerator: makeAddFn(10)
    }, 

    {
        id: "a6",
        title: "25 + 25",
        description: "Adding two numbers between 0 and 25",
        timeLimit: 45,
        questionsTotal: 15,
        questionGenerator: makeAddFn(25)
    },

    {
        id: "s1",
        title: "10 - 10",
        description: "Subtracting two numbers between 0 and 10",
        timeLimit: 30,
        questionsTotal: 15,
        questionGenerator: makeSubFn(10)
    },

    {
        id: "s6",
        title: "25 - 25",
        description: "Subtracting two numbers between 0 and 25",
        timeLimit: 45,
        questionsTotal: 20,
        questionGenerator: makeSubFn(25)
    }, 

    {
        id: "m1",
        title: "5 * 5",
        description: "Multiplying two numbers between 0 and 5",
        timeLimit: 30,
        questionsTotal: 15,
        questionGenerator: makeMultFn(5)
    },

    {
        id: "m3",
        title: "10 * 10",
        description: "Multiplying two numbers between 0 and 10",
        timeLimit: 45,
        questionsTotal: 20,
        questionGenerator: makeMultFn(10)
    },

    {
        id: "test",
        title: "one question",
        description: "Adding two numbers between 0 and 6",
        timeLimit: 30,
        questionsTotal: 2,
        questionGenerator: makeAddFn(6)
    }
];

export default Levels; 