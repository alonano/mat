import QuizGenerator from "./QuizGenerator.js";

describe("Expression generator", () => {
    let generateAnswerMock;
    let generatorFn;

    const expectQuestionIsCorrect = (question) => {
        expect(question).toBeDefined();
        expect(question.text).toBeDefined();
        expect(question.text.length > 2).toBeTruthy();
        expect(question.answers).toHaveLength(4);
        expect(question.correctAnswer).toBeDefined();
    };

    const expectAllAnswers = (expectedAnswers, question) => {
        let allFound = expectedAnswers.every(e => question.answers.indexOf(e) !== -1);
        expect(allFound).toBeTruthy();
    };

    const questionFn = (questionAnswerArray) => {
        generateAnswerMock = jest.fn();
        let i = 0; 
        while (i < questionAnswerArray.length - 1) {
            let question = {
                question: questionAnswerArray[i],
                answer: questionAnswerArray[i + 1]
            };
            generateAnswerMock.mockReturnValueOnce(question);
            i += 2;
        } 
        generatorFn = () => QuizGenerator(generateAnswerMock);
        return generatorFn;
    };

    beforeEach(() => {
    });

    it("generates quiz question object", () => {
        let generate = questionFn([
            "1 + 2", "3",
            "3 + 4", "7", 
            "5 + 6", "11",
            "6 + 2", "8" 
        ]);
        let question = generate();
        expectQuestionIsCorrect(question);
        expectAllAnswers(["3", "7", "11", "8"], question)
    });

    it("throws an exception if generator function does not generate various results", () => {
        const constGeneratorFn = () => ({question: "1 + 1", answer: "2"});
        try {
            QuizGenerator(constGeneratorFn);
            expect(false).toBeTruthy(); // should not reach here
        } catch (e) {
            expect(e.message).toEqual("Not able to generate wrong answers");
        }
    });

    it("generates all different answers, even when generatorFn repeats itself", () => {
        let generate = questionFn([
            "1 + 2", "3",
            "3 + 4", "3", 
            "5 + 6", "11",
            "6 + 2", "8",
            "5 + 4", "9" 
        ]);
        expectQuestionIsCorrect(generate());
    });

    it("generates different questions in three subsequent calls", () => {
        let generate = questionFn([
            "1 + 2", "3",
            "3 + 4", "7", 
            "5 + 6", "11",
            "6 + 2", "8", 
            "1 + 4", "5",
            "3 + 4", "9", 
            "6 + 6", "12",
            "6 + 2", "2", 
            "5 + 2", "1",
            "3 + 4", "10", 
            "5 + 6", "14",
            "6 + 2", "4" 
        ]);
        let question1 = generate().text;
        let question2 = generate().text; 
        let question3 = generate().text; 
        expect(question1).not.toEqual(question2);
        expect(question1).not.toEqual(question3);
        expect(question2).not.toEqual(question3);
    });

    it("should not have the same answer position in three subsequent calls", () => {
        let generate = questionFn([
            "1 + 2", "3",
            "3 + 4", "7", 
            "5 + 6", "11",
            "6 + 2", "8", 
            "1 + 2", "3",
            "3 + 4", "7", 
            "5 + 6", "11",
            "6 + 2", "8", 
            "1 + 2", "3",
            "3 + 4", "7", 
            "5 + 6", "11",
            "6 + 2", "8" 
        ]);
        let b1 = generate().correctAnswer; 
        let b2 = generate().correctAnswer; 
        let b3 = generate().correctAnswer;
        expect(b1 !== b2 || b2 !== b3 || b3 !== b1).toBeTruthy();
    });
});