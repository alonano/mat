import Utils from "./Utils";

const QuizGenerator = (questionGeneratorFn) => {
    const howManyAnswers = 4;
    const addedUnique = (a, element) => {
        if (a.every(e => e !== element)) {
            a.push(element);
            return true;
        } else {
            return false;
        }
    };

    let q = questionGeneratorFn();
    let answers = [q.answer];
    let positionOfCorrect = Utils.rnd(0, 3);
    while (answers.length < howManyAnswers) {
        let remainingAttempts = 100;
        while (!addedUnique(answers, questionGeneratorFn().answer)) {
            remainingAttempts--;
            if (remainingAttempts <= 0) {
                throw Error("Not able to generate wrong answers");
            }
        }
    }
    let temp = answers[positionOfCorrect];
    answers[positionOfCorrect] = answers[0];
    answers[0] = temp;
    let response = {
        text: q.question,
        answers: answers,
        correctAnswer: positionOfCorrect
    };
    return response;
};

export default QuizGenerator;