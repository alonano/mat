import Utils from "../Utils";

const makeAddFn = (max) => {
    const f = () => {
        let number1 = Utils.rnd(0, max);
        let number2 = Utils.rnd(0, max);
        return {
            question: number1 + " + " + number2,
            answer: (number1 + number2).toString()
        };
    }
    return f;
}

export default makeAddFn; 