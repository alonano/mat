import Utils from "../Utils";

const makeSubFn = (max) => {
    const f = () => {
        let number1 = Utils.rnd(3, max);
        let number2 = Utils.rnd(0, number1);
        return {
            question: number1 + " - " + number2,
            answer: (number1 - number2).toString()
        };
    }
    return f;
}

export default makeSubFn; 